package net.chenlin.dp.ids.common.util;

/**
 * 公共工具类
 * @author zcl<yczclcn@163.com>
 */
public class CommonUtil {

    /**
     * 字符串是否为空
     * @param value
     * @return
     */
    public static boolean strIsEmpty(CharSequence value) {
        return value == null || value.length() == 0;
    }

    /**
     * 字符串是否为空
     * @param value
     * @return
     */
    public static boolean strIsNotEmpty(CharSequence value) {
        return !strIsEmpty(value);
    }

}
