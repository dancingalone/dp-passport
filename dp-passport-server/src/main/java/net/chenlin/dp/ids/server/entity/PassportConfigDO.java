package net.chenlin.dp.ids.server.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * ids服务端配置DO
 * @author zhouchenglin[yczclcn@163.com]
 */
public class PassportConfigDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 记录id */
    private Long id;

    /** 参数编码 */
    private String macroKey;

    /** 参数配置值 */
    private String macroValue;

    /** 参数描述 */
    private String remark;

    /** 创建人 */
    private Long createUser;

    /** 修改人 */
    private Long updateUser;

    /** 创建时间 */
    private Date createTime;

    /** 修改时间 */
    private Date updateTime;

    /**
     * constructor
     */
    public PassportConfigDO() {
        super();
    }

    /**
     * getter for id
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * setter for id
     *
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for macroKey
     *
     * @return macroKey
     */
    public String getMacroKey() {
        return macroKey;
    }

    /**
     * setter for macroKey
     *
     * @param macroKey macroKey
     */
    public void setMacroKey(String macroKey) {
        this.macroKey = macroKey;
    }

    /**
     * getter for macroValue
     *
     * @return macroValue
     */
    public String getMacroValue() {
        return macroValue;
    }

    /**
     * setter for macroValue
     *
     * @param macroValue macroValue
     */
    public void setMacroValue(String macroValue) {
        this.macroValue = macroValue;
    }

    /**
     * getter for remark
     *
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * setter for remark
     *
     * @param remark remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * getter for createUser
     *
     * @return createUser
     */
    public Long getCreateUser() {
        return createUser;
    }

    /**
     * setter for createUser
     *
     * @param createUser createUser
     */
    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    /**
     * getter for updateUser
     *
     * @return updateUser
     */
    public Long getUpdateUser() {
        return updateUser;
    }

    /**
     * setter for updateUser
     *
     * @param updateUser updateUser
     */
    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * getter for createTime
     *
     * @return createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * setter for createTime
     *
     * @param createTime createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * getter for updateTime
     *
     * @return updateTime
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * setter for updateTime
     *
     * @param updateTime updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * to string
     * @return
     */
    @Override
    public String toString() {
        return "PassportConfigDO{" +
                "id=" + id +
                ", macroKey='" + macroKey + '\'' +
                ", macroValue='" + macroValue + '\'' +
                ", remark='" + remark + '\'' +
                ", createUser=" + createUser +
                ", updateUser=" + updateUser +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

}
