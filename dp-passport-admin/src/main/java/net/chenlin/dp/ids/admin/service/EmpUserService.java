package net.chenlin.dp.ids.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.chenlin.dp.ids.admin.entity.EmpUserDO;

import java.util.Map;

/**
 * passport用户api
 * @author zhouchenglin[yczclcn@163.com]
 */
public interface EmpUserService {

    /**
     * 分页查询
     * @param param
     * @return
     */
    Page<EmpUserDO> listPage(Map<String, Object> param);

}
